import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

const projectOutterMargin =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(left: 17, right: 17, top: 16);

const projectMargin =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(left: 17, right: 17, top: 16);

const projectMarginHorizontal =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(left: 17, right: 17);

const projectMarginBtn =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(top: 12, bottom: 12);

const projectDashboardPadding =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(top: 16, bottom: 16);

const projectOutterPadding =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(top: 16, right: 24, left: 24);

const projectInnerPadding = (kIsWeb)
    ? EdgeInsets.zero
    : EdgeInsets.only(top: 16, right: 12, left: 12, bottom: 16);

const projectSearchFieldPadding =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(top: 16, right: 17, left: 17);

const projectParagraph = (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(left: 8);

const projectBoxedParagraph =
    (kIsWeb) ? EdgeInsets.zero : EdgeInsets.only(left: 8, top: 16, bottom: 16);

const projectDialogPadding = (kIsWeb)
    ? EdgeInsets.zero
    : EdgeInsets.only(top: 30, right: 24, left: 24, bottom: 20);

const projectOutterPadded = EdgeInsets.only(top: 0, right: 16, left: 16);
