import 'package:flutter/material.dart';
// import 'package:project_web/helper/app_scale.dart';
import 'package:si_pekan/theme/colors.dart';

class LoadingIndicator extends StatelessWidget {
  final double? height;
  const LoadingIndicator({Key? key, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      child: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 9.0),
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(projectPrimary),
          ),
        ),
      ),
    );
  }
}
