import 'package:flutter/material.dart';
import 'package:si_pekan/helper/app_scale.dart';
import 'package:si_pekan/theme/button_style.dart';
import 'package:si_pekan/theme/colors.dart';

class ButtonConfirm extends StatefulWidget {
  final Function()? onTap;
  final String text;
  final double? width;
  final double? fontSize;

  const ButtonConfirm({
    Key? key,
    this.onTap,
    required this.text,
    this.width,
    this.fontSize,
  }) : super(key: key);

  @override
  _ButtonConfirmState createState() => _ButtonConfirmState();
}

class _ButtonConfirmState extends State<ButtonConfirm> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width ?? context.deviceWidth(),
      child: ButtonTheme(
        child: TextButton(
          onPressed: widget.onTap,
          style: confirmStyle(widget.onTap != null),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              widget.text,
              style: TextStyle(
                  fontSize: context.scaleFont(widget.fontSize ?? 14),
                  color: widget.onTap != null ? projectWhite : projectBlack,
                  fontWeight: FontWeight.bold),
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonCancel extends StatefulWidget {
  final Function()? onTap;
  final String text;
  final double? width;
  final double? fontSize;

  const ButtonCancel({
    Key? key,
    this.onTap,
    required this.text,
    this.width,
    this.fontSize,
  }) : super(key: key);

  @override
  _ButtonCancelState createState() => _ButtonCancelState();
}

class _ButtonCancelState extends State<ButtonCancel> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width ?? context.deviceWidth(),
      child: ButtonTheme(
        child: TextButton(
          onPressed: widget.onTap,
          style: cancelStyle(widget.onTap != null),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text(
              widget.text,
              style: TextStyle(
                  fontSize: context.scaleFont(widget.fontSize ?? 14),
                  color: widget.onTap != null ? projectPrimary : projectBlack,
                  fontWeight: FontWeight.bold),
              maxLines: 1,
            ),
          ),
        ),
      ),
    );
  }
}
