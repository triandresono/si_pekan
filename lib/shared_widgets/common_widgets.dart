import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:si_pekan/helper/constant.dart';

class ResponsiveList extends StatefulWidget {
  final List<Widget> children;
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment mainAxisAlignment;
  final MainAxisSize mainAxisSize;
  const ResponsiveList({
    Key? key,
    required this.children,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.mainAxisSize = MainAxisSize.max,
  }) : super(key: key);

  @override
  State<ResponsiveList> createState() => _ResponsiveListState();
}

class _ResponsiveListState extends State<ResponsiveList> {
  @override
  Widget build(BuildContext context) {
    return (kIsWeb && !isWebMobile)
        ? Row(
            crossAxisAlignment: widget.crossAxisAlignment,
            mainAxisAlignment: widget.mainAxisAlignment,
            mainAxisSize: widget.mainAxisSize,
            children: widget.children,
          )
        : Column(
            crossAxisAlignment: widget.crossAxisAlignment,
            mainAxisAlignment: widget.mainAxisAlignment,
            mainAxisSize: widget.mainAxisSize,
            children: widget.children,
          );
  }
}
