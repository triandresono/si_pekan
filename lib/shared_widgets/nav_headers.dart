import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si_pekan/helper/clocks.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/helper/locator.dart';
import 'package:si_pekan/helper/navigator_service.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:si_pekan/theme/icons.dart';
import 'package:si_pekan/helper/app_scale.dart';
import 'package:intl/intl.dart';

AppBar navHeader({String? titleAppBar, List<Widget>? actions}) {
  return AppBar(
    elevation: 0,
    title: Builder(builder: (context) {
      return Text(
        titleAppBar ?? 'Sistem Informasi Perpustakaan',
        style: TextStyle(
          fontWeight: FontWeight.w300,
          color: projectWhite,
          fontSize: context.scaleFont(15),
        ),
      );
    }),
    leading: (kIsWeb && !isWebMobile)
        ? null
        : Builder(
            builder: (context) => IconButton(
              icon: const Icon(
                MyFlutterApp.menu2outline,
                color: projectWhite,
              ),
              onPressed: () => Scaffold.of(context).openDrawer(),
              tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
            ),
          ),
    backgroundColor: projectPrimary,
  );
}

AppBar simpleHeader(String titleAppBar, {Function()? onBackPressed}) {
  return AppBar(
    title: Text(
      titleAppBar,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: projectPrimary,
      ),
    ),
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(
          Icons.arrow_back,
          color: projectPrimary,
        ),
        onPressed: onBackPressed ?? () => locator<NavigatorService>().goBack(),
      ),
    ),
    backgroundColor: projectWhite,
  );
}

AppBar navHeaderTab(
    String titleAppBar, TabController controller, List<Widget> tabList) {
  return AppBar(
    title: Text(
      titleAppBar,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
        fontSize: 14,
      ),
    ),
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(
          Icons.menu,
          color: Colors.black,
        ),
        onPressed: () => Scaffold.of(context).openDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
    backgroundColor: Colors.blueAccent,
    bottom: TabBar(
        controller: controller,
        tabs: tabList,
        labelColor: Colors.blueAccent,
        labelStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        unselectedLabelColor: Colors.grey,
        indicatorColor: Colors.black),
  );
}

AppBar navHeaderSearch({List<Widget>? actions, Widget? searchBar}) {
  return AppBar(
    title: searchBar,
    leading: Builder(
      builder: (context) => IconButton(
        icon: const Icon(
          Icons.menu,
          color: Colors.white,
        ),
        onPressed: () => Scaffold.of(context).openDrawer(),
        tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
      ),
    ),
    backgroundColor: Colors.lightBlue,
    actions: actions,
  );
}

// AppBar navComment(String titleAppBar,
//     {List<Widget> actions, dynamic returnval}) {
//   return AppBar(
//     title: Text(
//       titleAppBar,
//       style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
//     ),
//     leading: Container(
//       margin: EdgeInsets.only(right: 10),
//       height: 40,
//       width: 40,
//       child: Center(child: BtnBack(returnval: returnval)),
//     ),
//     backgroundColor: projectWhite,
//   );
// }

AppBar jagaBar() {
  final DateTime now = DateTime.now();
  final DateFormat formatter = DateFormat('yMMMMd');
  final String formatted = formatter.format(now);
  return AppBar(
    // flexibleSpace: const Image(
    //   image: AssetImage(imgPath + 'bar_mask.png'),
    //   fit: BoxFit.cover,
    // ),
    // toolbarHeight: (kIsWeb && !isWebMobile) ? 6.5.wh : null,
    elevation: 0,

    // (kIsWeb && !isWebMobile)
    //     ? null
    //     : Builder(
    //         builder: (context) => IconButton(
    //           icon: Icon(
    //             MyFlutterApp.menu2outline,
    //             color: projectWhite,
    //             size: context.scaleFont(25),
    //           ),
    //           onPressed: () => Scaffold.of(context).openDrawer(),
    //           tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
    //         ),
    //       ),
    title: Builder(
      builder: (context) => Builder(
        builder: (context) => Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(10.0),
              decoration: const BoxDecoration(
                  color: projectWhite,
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Text(
                'SI PEKAN',
                style: TextStyle(
                  color: projectPrimary,
                  fontSize: context.scaleFont(20),
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    actions: [
      Builder(
        builder: (context) => Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              formatted,
              style: TextStyle(
                color: projectWhite,
                fontSize: context.scaleFont(16),
                fontWeight: FontWeight.w300,
              ),
            ),
            const SizedBox(width: 8),
            Container(
              height: 30,
              width: 1,
              color: projectWhite,
            ),
            const SizedBox(width: 8),
            const Padding(
              padding: EdgeInsets.only(right: 12.0),
              child: Clocks(),
            ),
          ],
        ),
      )
    ],
    backgroundColor: projectPrimary,
  );
}
