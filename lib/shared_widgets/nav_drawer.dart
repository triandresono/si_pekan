import 'package:auto_route/auto_route.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:si_pekan/helper/app_scale.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/shared_widgets/expandeds.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:si_pekan/theme/icons.dart';

class NavDrawer extends StatefulWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: projectWhite,
      width: MediaQuery.of(context).size.width * 0.62,
      child: Drawer(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 1.0),
            child: Column(
              children: [
                const SizedBox(
                  height: 5,
                ),
                ChildTile(
                  icon: MyFlutterApp.homeoutline,
                  text: 'Home',
                  onTap: () {
                    context.navigateNamedTo(Routes.dashboard);
                  },
                ),
                HeaderTile(
                  icon: MyFlutterApp.personoutline,
                  text: 'Profile',
                  childrens: [
                    ChildTile(
                      text: 'Fasilitas',
                      onTap: () {
                        // context.navigateNamedTo(Routes.usersPage);
                      },
                    ),
                    ChildTile(
                      text: 'Tentang Kami',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: Icons.call,
                  text: 'Layanan',
                  childrens: [
                    ChildTile(
                      text: 'Daring',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Peraturan',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Jam Buka',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Keanggotaan',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: Icons.collections,
                  text: 'Koleksi',
                  childrens: [
                    ChildTile(
                      text: 'Katalog',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Digital',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Jenis Media',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Buku',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: MyFlutterApp.activityoutline,
                  text: 'Kegiatan',
                  childrens: [
                    ChildTile(
                      text: 'Berita',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Galeri',
                      onTap: () {},
                    ),
                  ],
                ),
                HeaderTile(
                  icon: MyFlutterApp.awardoutline,
                  text: 'Penghargaan',
                  childrens: [
                    ChildTile(
                      text: 'Pustakawan',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Rating Peminjam',
                      onTap: () {},
                    ),
                    ChildTile(
                      text: 'Rating Pengunjung',
                      onTap: () {},
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget? navDrawer() {
  if (kIsWeb && !isWebMobile) {
    return null;
  } else {
    return const NavDrawer();
  }
}
