import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:si_pekan/helper/app_scale.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/shared_widgets/expandable_drawer.dart';
import 'package:si_pekan/shared_widgets/expandeds.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:si_pekan/theme/icons.dart';

class PersistDrawer extends StatefulWidget {
  const PersistDrawer({Key? key}) : super(key: key);

  @override
  _PersistDrawerState createState() => _PersistDrawerState();
}

class _PersistDrawerState extends State<PersistDrawer> {
  bool isExpanded = true;
  List<bool> initial = [];

  @override
  void initState() {
    for (int i = 0; i < 5; i++) {
      initial.add(false);
    }
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isExpanded
        ? ExpandableWidget(
            direction: Axis.horizontal,
            expand: isExpanded,
            child: Container(
              color: projectWhite,
              width: context.deviceWidth() * 0.17,
              child: Drawer(
                elevation: 0,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      AppBar(
                        automaticallyImplyLeading: false,
                        elevation: 0,
                        titleSpacing: 0,
                        backgroundColor: projectPrimary,
                        title: Container(
                          // padding: const EdgeInsets.symmetric(horizontal: 8),
                          color: projectPrimary,
                          child: Row(
                            children: const [
                              Expanded(
                                child: ChildTile(
                                  icon: MyFlutterApp.menu2outline,
                                  text: 'Menu',
                                  color: projectWhite,
                                ),
                              ),
                            ],
                          ),
                        ),
                        actions: [
                          Padding(
                            padding: const EdgeInsets.only(right: 8.0),
                            child: IconButton(
                                onPressed: () {
                                  setState(() {
                                    isExpanded = !isExpanded;
                                    for (int i = 0; i < initial.length; i++) {
                                      initial[i] = false;
                                    }
                                  });
                                },
                                icon: const Icon(Icons.close)),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      ChildTile(
                        icon: MyFlutterApp.homeoutline,
                        text: 'Home',
                        onTap: () {
                          context.navigateNamedTo(Routes.dashboard);
                        },
                      ),
                      HeaderTile(
                        initialExpanded: initial[0],
                        icon: MyFlutterApp.personoutline,
                        text: 'Profile',
                        childrens: [
                          ChildTile(
                            text: 'Fasilitas',
                            onTap: () {
                              // context.navigateNamedTo(Routes.usersPage);
                            },
                          ),
                          ChildTile(
                            text: 'Tentang Kami',
                            onTap: () {},
                          ),
                        ],
                      ),
                      HeaderTile(
                        initialExpanded: initial[1],
                        icon: Icons.call,
                        text: 'Layanan',
                        childrens: [
                          ChildTile(
                            text: 'Daring',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Peraturan',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Jam Buka',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Keanggotaan',
                            onTap: () {},
                          ),
                        ],
                      ),
                      HeaderTile(
                        initialExpanded: initial[2],
                        icon: Icons.collections,
                        text: 'Koleksi',
                        childrens: [
                          ChildTile(
                            text: 'Katalog',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Digital',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Jenis Media',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Buku',
                            onTap: () {},
                          ),
                        ],
                      ),
                      HeaderTile(
                        initialExpanded: initial[3],
                        icon: MyFlutterApp.activityoutline,
                        text: 'Kegiatan',
                        childrens: [
                          ChildTile(
                            text: 'Berita',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Galeri',
                            onTap: () {},
                          ),
                        ],
                      ),
                      HeaderTile(
                        initialExpanded: initial[4],
                        icon: MyFlutterApp.awardoutline,
                        text: 'Penghargaan',
                        childrens: [
                          ChildTile(
                            text: 'Pustakawan',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Rating Peminjam',
                            onTap: () {},
                          ),
                          ChildTile(
                            text: 'Rating Pengunjung',
                            onTap: () {},
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ))
        : Container(
            color: projectWhite,
            width: MediaQuery.of(context).size.width * 0.035,
            child: Drawer(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    AppBar(
                        automaticallyImplyLeading: false,
                        elevation: 0,
                        titleSpacing: 0,
                        backgroundColor: projectPrimary,
                        title: ListTile(
                          onTap: () {
                            setState(() {
                              isExpanded = !isExpanded;
                            });
                          },
                          title: Icon(
                            MyFlutterApp.menu2outline,
                            size: context.scaleFont(15),
                            color: projectWhite,
                          ),
                        )),
                    const SizedBox(
                      height: 20,
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.homeoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        context.navigateNamedTo(Routes.dashboard);
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.personoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[0] = true;
                        });
                      },
                    ),
                    // const HoverTile(icon: MyFlutterApp.personoutline),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        Icons.call,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[1] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        Icons.collections,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[2] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.activityoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[3] = true;
                        });
                      },
                    ),
                    ListTile(
                      title: const SizedBox(width: 1),
                      leading: Icon(
                        MyFlutterApp.awardoutline,
                        size: context.scaleFont(15),
                      ),
                      onTap: () {
                        setState(() {
                          isExpanded = true;
                          initial[4] = true;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          );
  }
}
