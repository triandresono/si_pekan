import 'package:shared_preferences/shared_preferences.dart';
import 'package:si_pekan/helper/app_exception.dart';
import 'package:si_pekan/helper/constant.dart';

class DatabaseHelper {
  DatabaseHelper.internal();
  static final DatabaseHelper _instance = DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;
  SharedPreferences? prefs;

  Future<dynamic> get db async {
    prefs = await SharedPreferences.getInstance();
    bool isEmpty = await isLogged();
    if (!isEmpty) {
      throw EmptyDatabase();
    }
  }

  Future<bool> isLogged() async {
    prefs = await SharedPreferences.getInstance();
    String? log = prefs?.getString(DBKey.login);
    return log != null || log!.isNotEmpty;
  }
}
