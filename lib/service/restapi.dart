import 'package:flutter/cupertino.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/service/api_url.dart';
import 'package:si_pekan/service/net_util.dart';

class RestApi extends UrlAPI {
  // ignore: unused_field
  NetworkUtil util = NetworkUtil();

  Future<dynamic> login(
      {required Map<String, dynamic> body, Map<String, String>? param}) async {
    dynamic value = await util.testPost(
      vccIdentity + "/login",
      body: body,
      param: param,
      headers: commonHeader,
    );
    debugPrint(value.toString());
    if (value['status'] != Constant.statusSucces) throw value['message'];
    return value['data'];
  }

  Future<dynamic> logout(
      {required Map<String, dynamic> body, Map<String, String>? param}) {
    return util
        .post(
      vccIdentity + "/logout",
      body: body,
      param: param,
      headers: commonHeader,
    )
        .then((value) {
      debugPrint(value.toString());
      if (value['status'] != Constant.statusSucces) throw value['message'];
      return value['data'];
    });
  }

  Future<dynamic> refreshToken(
      {required Map<String, dynamic> body, Map<String, String>? param}) {
    return util
        .post(
      vccIdentity + "/refresh-token",
      body: body,
      param: param,
      headers: commonHeader,
    )
        .then((value) {
      debugPrint(value.toString());
      if (value['status'] != Constant.statusSucces) throw value['message'];
      return value['data'];
    });
  }
}
