import 'package:flutter/material.dart';
import 'package:si_pekan/screens/login_screens/login_page.dart';
import 'package:si_pekan/shared_widgets/nav_headers.dart';
import 'constant.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constant.menuLogin:
        return MaterialPageRoute(builder: (_) => const LoginPage());
      case Constant.menuDashboard:
        return MaterialPageRoute(builder: (_) => Container());

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: navHeader(),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Ups, ada sesuatu yang salah. Coba lagi"),
            Container(
              margin: const EdgeInsets.all(4),
            ),
            GestureDetector(onTap: () {}, child: const Icon(Icons.replay))
          ],
        )),
      );
    });
  }
}
