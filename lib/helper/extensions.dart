import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension SnackBar on BuildContext {
  dynamic succesSnackbar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(succesSnackbar(message));
  }

  dynamic failSnackbar(String message) {
    ScaffoldMessenger.of(this).showSnackBar(failSnackbar(message));
  }
}
