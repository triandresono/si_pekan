import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:timer_builder/timer_builder.dart';
import 'package:si_pekan/helper/app_scale.dart';

class Clocks extends StatefulWidget {
  const Clocks({Key? key}) : super(key: key);

  @override
  _ClocksState createState() => _ClocksState();
}

class _ClocksState extends State<Clocks> {
  @override
  Widget build(BuildContext context) {
    return TimerBuilder.periodic(const Duration(seconds: 1),
        builder: (context) {
      return Text(
        getSystemTime(),
        style: TextStyle(
          color: projectWhite,
          fontSize: context.scaleFont(16),
          fontWeight: FontWeight.w300,
        ),
      );
    });
  }

  String getSystemTime() {
    var now = DateTime.now();
    return DateFormat("H:m:s").format(now);
  }
}
