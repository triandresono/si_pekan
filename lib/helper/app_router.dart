import 'package:auto_route/auto_route.dart';
import 'package:si_pekan/screens/dashboard/dashboard_page.dart';
import 'package:si_pekan/screens/home_screens/home_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      path: '/home',
      page: HomePage,
      initial: true,
      children: [
        CustomRoute(
          path: 'dashboard',
          page: DashBoardPage,
          initial: true,
          transitionsBuilder: TransitionsBuilders.slideRightWithFade,
          maintainState: false,
        ),
        // CustomRoute(
        //   path: 'users',
        //   page: UsersPage,
        //   transitionsBuilder: TransitionsBuilders.slideRightWithFade,
        //   maintainState: false,
        // ),
        // AutoRoute(path: 'settings', page: SettingsPage),
      ],
    ),
    // AutoRoute(path: '/login', page: LoginPage, initial: true)
  ],
)
class $AppRouter {}
