import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

final isWebMobile = kIsWeb &&
    (defaultTargetPlatform == TargetPlatform.iOS ||
        defaultTargetPlatform == TargetPlatform.android);

class Constant extends InheritedWidget {
  static Constant? of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<Constant>();

  const Constant({required Widget child, Key? key})
      : super(key: key, child: child);
  //& MENUS
  static const String menuLogin = 'LOGIN';
  static const String menuDashboard = 'DASHBOARD';

  //& STATUS
  static const String statusSucces = 'success';
  static const String statusError = 'error';

  @override
  bool updateShouldNotify(Constant oldWidget) => false;
}

class DBKey {
  static const login = 'login';
}

class Routes {
  static const String loginPage = '/login';
  static const String homePage = '/home';
  static const String dashboard = '/home/dashboard';
  static const String usersPage = '/home/users';
}

const String imgPath = 'assets/img/';
const String iconPath = 'assets/icons/';
