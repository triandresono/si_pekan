import 'package:flutter/material.dart';

import 'locator.dart';

class NavigatorService {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState!.pushNamed(routeName);
  }

  Future<dynamic> navigateToWithArgmnt(String routeName, dynamic obj) {
    return navigatorKey.currentState!.pushNamed(routeName, arguments: obj);
  }

  Future<dynamic> navigateReplaceTo(String routeName) {
    return navigatorKey.currentState!.pushReplacementNamed(routeName);
  }

  void goBack({value}) {
    return navigatorKey.currentState!.pop(value);
  }
}

class Jump {
  static Future<dynamic> navigateTo(String routeName) {
    return locator<NavigatorService>().navigateTo(routeName);
  }

  static Future<dynamic> navigateToWithArgmnt(String routeName, dynamic obj) {
    return locator<NavigatorService>().navigateToWithArgmnt(routeName, obj);
  }

  static Future<dynamic> navigateReplaceTo(String routeName) {
    return locator<NavigatorService>().navigateReplaceTo(routeName);
  }

  static goBack() {
    return locator<NavigatorService>().goBack();
  }
}
