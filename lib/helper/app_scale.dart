import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:si_pekan/helper/constant.dart';

extension Scaler on BuildContext {
  double scaleFont(double initialFontSize) {
    return (MediaQuery.of(this).size.width * 0.0027) * scaler(initialFontSize);
  }

  double scaleHeight(double initialHeight) {
    return (MediaQuery.of(this).size.height * 0.0011) * initialHeight;
  }

  double deviceWidth() {
    return (MediaQuery.of(this).size.width);
  }

  double deviceHeight() {
    return (MediaQuery.of(this).size.height);
  }
}

double scaler(double x) {
  if (kIsWeb && !isWebMobile) {
    return x / 3.5;
  } else {
    return x;
  }
}

extension Modifier on num {
  double get wh {
    if (kIsWeb && !isWebMobile) {
      return this * 11;
    } else {
      return (this).toDouble();
    }
  }
}

extension Resp on num {
  double get resp {
    if (kIsWeb && !isWebMobile) {
      return this * 5;
    } else {
      return (this).toDouble();
    }
  }
}
