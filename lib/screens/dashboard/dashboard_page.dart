import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/shared_widgets/image_sliders.dart';
import 'package:si_pekan/shared_widgets/nav_drawer.dart';
import 'package:si_pekan/shared_widgets/nav_headers.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:si_pekan/theme/padding.dart';
import 'package:si_pekan/helper/app_scale.dart';

class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  _DashBoardPageState createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: navDrawer(),
      appBar: navHeader(),
      backgroundColor: projectBG,
      body: const DashboardBody(),
    );
  }
}

class DashboardBody extends StatefulWidget {
  const DashboardBody({Key? key}) : super(key: key);

  @override
  _DashboardBodyState createState() => _DashboardBodyState();
}

class _DashboardBodyState extends State<DashboardBody> {
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: projectOutterPadded,
        height: context.deviceHeight() * 3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            const SizedBox(height: 8),
            SizedBox(
              child: Row(
                children: const [
                  Expanded(
                    child: CarouselWithIndicatorDemo(),
                  ),
                ],
              ),
            ),

            Expanded(
              flex: (kIsWeb && !isWebMobile) ? 3 : 2,
              child: Container(),
            ),
            Spacer(flex: (kIsWeb && !isWebMobile) ? 8 : 4),
            // Expanded(
            //   child: BarChartSample5(),
            // ),
            // Spacer(flex: 2),
          ],
        ),
      ),
    );
  }
}
