import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:si_pekan/helper/app_scale.dart';
import 'package:si_pekan/helper/extensions.dart';
import 'package:si_pekan/helper/constant.dart';
import 'package:si_pekan/helper/navigator_service.dart';
import 'package:si_pekan/shared_widgets/common_buttons.dart';
import 'package:si_pekan/shared_widgets/image_sliders.dart';
import 'package:si_pekan/shared_widgets/loading_indicator.dart';
import 'package:si_pekan/state_management/bloc/auth_bloc.dart';
import 'package:si_pekan/state_management/bloc/login_bloc.dart';
import 'package:si_pekan/state_management/event/auth_event.dart';
import 'package:si_pekan/state_management/state/auth_state.dart';
import 'package:si_pekan/state_management/state/login_state.dart';
import 'package:si_pekan/theme/colors.dart';
import 'package:si_pekan/theme/icons.dart';
import 'package:si_pekan/theme/padding.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: const Scaffold(
        backgroundColor: Colors.transparent,
        body: LoginBody(),
      ),
    );
  }
}

class LoginBody extends StatefulWidget {
  const LoginBody({Key? key}) : super(key: key);

  @override
  _LoginBodyState createState() => _LoginBodyState();
}

class _LoginBodyState extends State<LoginBody> {
  Future<bool> onBackPressed() {
    SystemNavigator.pop();
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: onBackPressed,
      child: SingleChildScrollView(
        child: SizedBox(
          height: deviceSize.height,
          width: deviceSize.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                flex: deviceSize.height > 600 ? 2 : 1,
                child: MultiBlocProvider(providers: [
                  BlocProvider<LoginBloc>(
                    create: (context) => LoginBloc(),
                  ),
                  BlocProvider<AuthBloc>(
                    create: (context) => AuthBloc()..add(AuthLogin()),
                  ),
                ], child: const LoginCard()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class LoginCard extends StatefulWidget {
  const LoginCard({
    Key? key,
  }) : super(key: key);

  @override
  _LoginCardState createState() => _LoginCardState();
}

class _LoginCardState extends State<LoginCard> {
  final GlobalKey<FormState> loginKey = GlobalKey();
  TextEditingController usernameCo = TextEditingController();
  TextEditingController passwordCo = TextEditingController();
  bool obscure = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<LoginBloc, LoginState>(
          listener: (context, state) async {
            if (state is LoginSuccess) {
              Jump.navigateTo(Constant.menuDashboard);
            }
            if (state is LoginFailed) {
              context.failSnackbar(state.error);
            }
          },
        ),
        BlocListener<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is AuthLoggedIn) {
              Jump.navigateTo(Constant.menuDashboard);
            }
            if (state is AuthError) {
              context.failSnackbar(state.message);
            }
          },
          child: Container(),
        )
      ],
      child: BlocBuilder<LoginBloc, LoginState>(
        builder: (context, state) {
          if (state is LoginInProgress) {
            return const LoadingIndicator();
          } else {
            return Container(
              constraints: const BoxConstraints(minHeight: 260),
              padding: projectOutterPadding,
              width: context.deviceWidth(),
              child: Form(
                key: loginKey,
                child: SingleChildScrollView(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Visibility(
                        visible: kIsWeb,
                        child: Expanded(
                          flex: 2,
                          child: Row(
                            children: const [
                              Expanded(
                                child: CarouselWithIndicatorDemo(),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const Visibility(visible: kIsWeb, child: Spacer()),
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 10.0),
                              child: Text(
                                'Sign In ',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: context.scaleFont(25),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 30.0),
                              child: Text(
                                'Welcome back, please login to your account',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: projectLightGray,
                                  fontSize: context.scaleFont(13),
                                ),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(bottom: 15.0),
                              child: Text(
                                'NIK',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            TextFormField(
                              controller: usernameCo,
                              decoration: InputDecoration(
                                prefixIcon: const Icon(
                                  MyFlutterApp.personoutline,
                                  color: projectFieldIcons,
                                ),
                                filled: true,
                                fillColor: projectFillField,
                                border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.circular(4)),
                              ),
                              keyboardType: TextInputType.emailAddress,
                              validator: (value) => "Silahkan isi username",
                            ),
                            const SizedBox(height: 30),
                            const Padding(
                              padding: EdgeInsets.only(bottom: 15.0),
                              child: Text(
                                'Password',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ),
                            TextFormField(
                              decoration: InputDecoration(
                                prefixIcon: const Icon(
                                  MyFlutterApp.lockoutline,
                                  color: projectFieldIcons,
                                ),
                                suffixIcon: GestureDetector(
                                  onTap: () => setState(
                                    () => obscure = !obscure,
                                  ),
                                  child: Icon(
                                    obscure
                                        ? MyFlutterApp.eyeoffoutline
                                        : MyFlutterApp.eyeoutline,
                                    color: projectFieldIcons,
                                  ),
                                ),
                                filled: true,
                                fillColor: projectFillField,
                                border: OutlineInputBorder(
                                    borderSide: BorderSide.none,
                                    borderRadius: BorderRadius.circular(4)),
                              ),
                              obscureText: obscure,
                              controller: passwordCo,
                              validator: (value) => "Silahkan isi password",
                            ),
                            const SizedBox(height: 30),
                            ButtonConfirm(
                              text: 'Sign In',
                              fontSize: 10,
                              onTap: () {
                                if (loginKey.currentState!.validate()) {}
                              },
                            ),
                            const SizedBox(height: 30),
                            Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: context.scaleHeight(2),
                                    // width: 20,
                                    color: projectLightGray,
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                      'Atau',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: projectLightGray,
                                        fontSize: context.scaleFont(10),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    height: context.scaleHeight(2),
                                    color: projectLightGray,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 30),
                            ButtonCancel(
                              text: 'Lapor Langsung Disini',
                              fontSize: 10,
                              onTap: () {
                                if (loginKey.currentState!.validate()) {}
                              },
                            ),
                          ],
                        ),
                      ),
                      const Visibility(visible: kIsWeb, child: Spacer()),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
